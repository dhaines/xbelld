# xbelld -- X daemon that performs an action every time the bell is rung

`xbelld` is a tiny utility to aid people who either don't like the default PC
speaker beep, or (like me) use an ALSA driver that doesn't yet have support
for the PC speaker (e.g. the `AD1981` chipset in the `snd_hda_intel` driver,
as of 2008-04-06).

`xbelld` performs a given action every time the X bell is rung. The actions
`xbelld` can currently perform include running a specified program, emulating
the PC speaker beep using your sound card (default), or playing a PCM encoded
WAVE file. The actions involving the sound card are only available if `xbelld`
is compiled with ALSA support. To convert your favourite files to a PCM
encoded WAVE file, use

    mplayer -vo null -vc null -ao pcm:fast:file=out.wav audio_file

`xbelld` can also throttle the bell if it is rung too often (e.g. some
terminal program goes crazy), and/or disable the audible bell (so that you
don't get the annoying PC speaker beep in addition to your `xbelld` action).

## INSTALLATION INSTRUCTIONS 

Dependencies: x11 (`libx11-dev`), alsa (`libasound2-dev`, optional), and
`pkg-config`.

Just type "make". There is no configure.

    vim features.mk
    make

## LICENCE

GPL-v3 or later. The full version of this licence can be obtained
[here](http://www.gnu.org/licenses/gpl.html).

## FORKS

* An OpenBSD fork (which is more of a rewrite) by *Marek Benc* is [here](https://github.com/dusxmt/nxbelld)

## AUTHOR

Gautam Iyer `<gi1242+xbelld@NoSPAM.com>` (replace `NoSPAM` with `gmail`)

Created  : Mon 07 Apr 2008 09:28:06 PM PDT

Modified : Sat 28 May 2016 10:34:35 PM EDT
