include features.mk
depends	:= x11 alsa
dev_packages := libx11-dev libasound2-dev
headers := xbelld.h
sources	:= xbelld.c
objects	:= $(sources:.c=.o)
distfiles := $(headers) $(sources) Makefile features.mk README xbelld.1 \
		ChangeLog
VERSION := 0.3.5

# Directory paths (taken from the GNU Make manual)
prefix		?= /usr/local
datarootdir	?= $(prefix)/share
datadir		?= $(datarootdir)
exec_prefix	?= $(prefix)
bindir		?= $(exec_prefix)/bin
libexecdir	?= $(exec_prefix)/libexec
mandir		?= $(datarootdir)/man
man1dir		?= $(mandir)/man1
INSTALL		?= /usr/bin/install -c
INSTALL_PROGRAM	?= $(INSTALL)
INSTALL_DATA	?= ${INSTALL} -m 644



CFLAGS := $(CFLAGS) -Wall -DVERSION=\"$(VERSION)\" -std=gnu99

ifdef DEBUG
    CFLAGS := $(filter-out -O%, $(CFLAGS))
    CFLAGS += -DDEBUG -g
else
    CFLAGS += -DNDEBUG
endif

ifdef WITHOUT_ALSA
    depends := $(filter-out alsa, $(depends))
    dev_packages := $(filter-out libasound2-dev, $(dev_packages))
else
    CFLAGS += -DHAVE_ALSA
endif

# Make sure that the dependencies we have exist
$(if $(shell pkg-config --exists $(depends) && echo 1), \
    ,\
    $(error Could not resolv dependencies: $(depends). On Debian based \
	systems try installing $(dev_packages).))

# Get correct CFLAGS / LDLIBS from dependencies.
CFLAGS += $(shell pkg-config $(depends) --cflags)
LDLIBS += $(shell pkg-config $(depends) --libs)
LDLIBS += -lm

%.o :  %.c $(headers) features.mk
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

xbelld: $(objects) features.mk
	$(CC) $(LDFLAGS) $(LOADLIBES) $(objects) $(LDLIBS) -o xbelld

.PHONY: clean
clean:
	rm -f *.o xbelld

ChangeLog:
	git2cl > ChangeLog

.PHONY: dist
dist: $(distfiles)
	export distdir=xbelld-$(VERSION)				\
	&& mkdir -p $$distdir						\
	&& cp $(distfiles) $$distdir/					\
	&& tar -c $$distdir | bzip2 > $$distdir.tbz2			\
	&& rm -rf $$distdir

.PHONY: install
install: xbelld
	$(INSTALL) -d $(DESTDIR)$(bindir) $(man1dir)
	$(INSTALL_PROGRAM) xbelld $(DESTDIR)$(bindir)
	$(INSTALL_DATA) xbelld.1 $(man1dir)
