#include <stdint.h>
#include <endian.h>
#include <byteswap.h>

#define APL_NAME "xBelld"

#define DEBUG_LEVEL (1)
#ifdef DEBUG
# define debug( level, fmt, ... )   \
    { \
	if( level <= DEBUG_LEVEL ) \
	    message( "%s:%d " fmt, __FILE__, __LINE__, ## __VA_ARGS__ ); \
    }
#else
# define debug( level, fmt, ... )
#endif /* DEBUG */

#ifdef HAVE_ALSA
/*
 * Wave file header.
 */
#if __BYTE_ORDER == __LITTLE_ENDIAN
# define COMPOSE_ID(a,b,c,d)	((a) | ((b)<<8) | ((c)<<16) | ((d)<<24))
# define LE_SHORT(v)		(v)
# define LE_INT(v)		(v)
# define BE_SHORT(v)		bswap_16(v)
# define BE_INT(v)		bswap_32(v)
#elif __BYTE_ORDER == __BIG_ENDIAN
# define COMPOSE_ID(a,b,c,d)	((d) | ((c)<<8) | ((b)<<16) | ((a)<<24))
# define LE_SHORT(v)		bswap_16(v)
# define LE_INT(v)		bswap_32(v)
# define BE_SHORT(v)		(v)
# define BE_INT(v)		(v)
#elif __BYTE_ORDER == __PDP_ENDIAN
# define COMPOSE_ID(a,b,c,d)	((c) | ((d)<<8) | ((a)<<16) | ((b)<<24))
# define LE_SHORT(v)		(v)
# define LE_INT(v)		(((v) >> 16) | ((v) << 16))
# define BE_SHORT(v)		bswap_16(v)
# define BE_INT(v)		((bswap_16((v) >> 16) << 16) | \
					bswap_16((v) & 65535))
#else
# error "Wrong endian"
#endif

typedef struct {
    /* Header */
    uint32_t magic;	/* Always "RIFF" */
    uint32_t file_len;	/* file length */
    uint32_t type;	/* Always "WAVE" */

    /* Format */
    uint32_t fmt;	/* Always "fmt " */
    uint32_t fmt_len;	/* Always 0x10 */
    uint16_t pcm_code;	/* Always 0x01 */
    uint16_t channels;	/* Number of channels */
    uint32_t rate;	/* Frequency of sample */
    uint32_t byte_psec; /* Bytes per second */
    uint16_t byte_pspl;	/* sample size; 1 or 2 bytes */
    uint16_t bit_pspl;	/* Bits per sample */

    /* Data */
    uint32_t data;	/* Always "data" */
    uint32_t data_len;	/* Data length */
} wave_header_t;
#endif /*HAVE_ALSA*/

void		message			( const char	*fmt, ... )
					__attribute__((
					    format( printf, 1, 2 )
					));
void		die			( const char *fmt, ... )
					__attribute__((
					    noreturn, format( printf, 1, 2 )
					));
#if 0
# define message( fmt, ... )	\
    error( 0, 0, fmt, ## __VA_ARGS__ )
# define errmessage( fmt, ... ) \
    error( 0, errno, fmt, ## __VA_ARGS__ )
# define fatalerr( fmt, ... )	\
    error( EXIT_FAILURE, errno, fmt, ## __VA_ARGS__ )
# define die( fmt, ... )	\
    error( EXIT_FAILURE, 0, fmt, ## __VA_ARGS__ )
#endif /*0*/

